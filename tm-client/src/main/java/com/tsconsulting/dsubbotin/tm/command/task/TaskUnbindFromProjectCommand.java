package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskUnbindFromProjectCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-unbind-from-project";
    }

    @Override
    @NotNull
    public String description() {
        return "Unbind task from project.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter project id:");
        @NotNull final String projectId = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByIdProject(session, projectId);
        TerminalUtil.printMessage("Enter task id:");
        @NotNull final String taskId = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().findByIdTask(session, taskId);
        endpointLocator.getProjectTaskEndpoint().unbindTaskFromProject(session, projectId, taskId);
        TerminalUtil.printMessage("[Task untied to project]");
    }

}
