package com.tsconsulting.dsubbotin.tm.component;

import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.stream.Collectors;

public final class FileScanner {

    private final static int INTERVAL = 5;

    @NotNull
    private final static String PATH = "./";

    @NotNull
    private final ScheduledExecutorService executorService;

    @NotNull
    private final List<String> commands;

    @NotNull
    private final Bootstrap bootstrap;

    public FileScanner(@NotNull Bootstrap bootstrap) {
        this.bootstrap = bootstrap;
        executorService = Executors.newSingleThreadScheduledExecutor();
        commands = new ArrayList<>();
    }

    public void start() {
        commands.addAll(bootstrap.getCommandService()
                .getArguments()
                .stream()
                .map(AbstractSystemCommand::name)
                .collect(Collectors.toList()));
        executorService.scheduleWithFixedDelay(this::run, 0, INTERVAL, TimeUnit.SECONDS);
    }

    private void run() {
        //исключаем файлы с расширением .log и .log.lck
        @NotNull final File[] files = new File(PATH).listFiles((dir, name) ->
                !(name.endsWith(".log") || name.endsWith(".lck")));
        for (@NotNull final File file : files) {
            if (!file.isFile()) continue;
            @NotNull String fileName = file.getName();
            if (fileName.lastIndexOf('.') != -1)
                fileName = fileName.substring(0, fileName.lastIndexOf('.')).toLowerCase();
            if (commands.contains(fileName)) {
                try {
                    bootstrap.runCommand(fileName);
                    if (!file.delete()) bootstrap.getLogService().info("Delete error!");
                } catch (Exception e) {
                    bootstrap.getLogService().error(e);
                }
            }
        }
    }

}
