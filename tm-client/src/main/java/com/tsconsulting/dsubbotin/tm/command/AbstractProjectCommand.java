package com.tsconsulting.dsubbotin.tm.command;

import com.tsconsulting.dsubbotin.tm.endpoint.Project;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public abstract class AbstractProjectCommand extends AbstractCommand {

    protected void showProject(@NotNull final Project project) {
        TerminalUtil.printMessage("Id: " + project.getId() + "\n" +
                "Name: " + project.getName() + "\n" +
                "Description: " + project.getDescription() + "\n" +
                "Status: " + project.getStatus().value() + "\n" +
                "Create date: " + project.getCreateDate() + "\n" +
                "Start date: " + project.getStartDate()
        );
    }

    protected String showProjectLine(@NotNull final Project project) {
        return String.format("%s - User Id: %s; Name: %s; Created: %s; Status: %s; Started: %s;",
                project.getId(),
                project.getUserId(),
                project.getName(),
                project.getCreateDate(),
                project.getStatus(),
                project.getStartDate());
    }

}
