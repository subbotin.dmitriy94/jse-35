package com.tsconsulting.dsubbotin.tm.command.data.fasterxml;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataXmlLoadFasterXmlCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-load-xml-fasterxml";
    }

    @Override
    public @NotNull String description() {
        return "Data loaded from XML using fasterxml.";
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().dataLoadXmlFasterXml(session);
        TerminalUtil.printMessage("Load completed.");
        TerminalUtil.printMessage("Logged out. Please log in.");
    }

}
