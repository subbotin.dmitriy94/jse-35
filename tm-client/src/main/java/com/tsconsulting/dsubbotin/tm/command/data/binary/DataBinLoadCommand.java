package com.tsconsulting.dsubbotin.tm.command.data.binary;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class DataBinLoadCommand extends AbstractCommand {

    @Override
    public @NotNull String name() {
        return "data-load-bin";
    }

    @Override
    public @NotNull String description() {
        return "Load binary data.";
    }

    @Override
    public void execute() {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        endpointLocator.getAdminEndpoint().loadBinary(session);
        TerminalUtil.printMessage("Load from binary completed.");

    }

}
