package com.tsconsulting.dsubbotin.tm.command.task;

import com.tsconsulting.dsubbotin.tm.command.AbstractTaskCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class TaskRemoveByIdCommand extends AbstractTaskCommand {

    @Override
    @NotNull
    public String name() {
        return "task-remove-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove task by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        endpointLocator.getTaskEndpoint().removeByIdTask(session, id);
        TerminalUtil.printMessage("[Task removed]");
    }

}
