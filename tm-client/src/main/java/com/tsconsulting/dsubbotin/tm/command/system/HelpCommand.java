package com.tsconsulting.dsubbotin.tm.command.system;

import com.tsconsulting.dsubbotin.tm.command.AbstractCommand;
import com.tsconsulting.dsubbotin.tm.command.AbstractSystemCommand;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;

public final class HelpCommand extends AbstractSystemCommand {

    @Override
    @NotNull
    public String name() {
        return "help";
    }

    @Override
    @NotNull
    public String arg() {
        return "-h";
    }

    @Override
    @NotNull
    public String description() {
        return "Display list of terminal commands.";
    }

    @Override
    public void execute() {
        for (@NotNull final AbstractCommand command : endpointLocator.getCommandService().getCommands()) {
            TerminalUtil.printMessage(command.toString());
        }
    }

}
