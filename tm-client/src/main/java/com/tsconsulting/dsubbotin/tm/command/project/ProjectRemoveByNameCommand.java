package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    @NotNull
    public String description() {
        return "Remove project by name.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().findByNameProject(session, name);
        endpointLocator.getProjectTaskEndpoint().removeProjectByName(session, name);
        TerminalUtil.printMessage("[Project removed]");
    }

}