package com.tsconsulting.dsubbotin.tm.command.project;

import com.tsconsulting.dsubbotin.tm.command.AbstractProjectCommand;
import com.tsconsulting.dsubbotin.tm.endpoint.Project;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public final class ProjectShowByIdCommand extends AbstractProjectCommand {

    @Override
    @NotNull
    public String name() {
        return "project-show-by-id";
    }

    @Override
    @NotNull
    public String description() {
        return "Display project by id.";
    }

    @Override
    public void execute() throws Exception {
        @Nullable Session session = endpointLocator.getSessionService().getSession();
        TerminalUtil.printMessage("Enter id:");
        @NotNull final String id = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findByIdProject(session, id);
        showProject(project);
    }

}
