package com.tsconsulting.dsubbotin.tm.exception.system;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class UnknownSortException extends AbstractException {

    public UnknownSortException() {
        super("Incorrect sort type entered!");
    }

}
