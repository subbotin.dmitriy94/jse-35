package com.tsconsulting.dsubbotin.tm;

import com.tsconsulting.dsubbotin.tm.endpoint.AbstractException_Exception;
import com.tsconsulting.dsubbotin.tm.endpoint.Session;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionEndpoint;
import com.tsconsulting.dsubbotin.tm.endpoint.SessionEndpointService;
import com.tsconsulting.dsubbotin.tm.marker.SoapCategory;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;

public class SessionEndpointTest {

    @NotNull
    private final SessionEndpoint sessionEndpoint;

    public SessionEndpointTest() {
        @NotNull final SessionEndpointService sessionEndpointService = new SessionEndpointService();
        sessionEndpoint = sessionEndpointService.getSessionEndpointPort();
    }

    @Test
    @Category(SoapCategory.class)
    public void openCloseSession() throws AbstractException_Exception {
        @NotNull final Session sessionTemp = sessionEndpoint.openSession("test", "test");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
    }

    @Test
    @Category(SoapCategory.class)
    public void register() throws AbstractException_Exception {
        @NotNull final Session sessionTemp = sessionEndpoint.register("temp", "temp", "a@a.com");
        Assert.assertNotNull(sessionTemp);
        Assert.assertTrue(sessionEndpoint.closeSession(sessionTemp));
    }

}
