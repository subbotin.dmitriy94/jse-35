package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDescriptionException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyNameException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import org.jetbrains.annotations.NotNull;

public final class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    public TaskService(@NotNull final ITaskRepository taskRepository) {
        super(taskRepository);
        this.taskRepository = taskRepository;
    }

    @NotNull
    @Override
    public Task create(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkName(name);
        if (EmptyUtil.isEmpty(description)) throw new EmptyDescriptionException();
        @NotNull Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        taskRepository.add(task);
        return task;
    }

    @Override
    public void clear(@NotNull final String userId) {
        taskRepository.clear(userId);
    }

    @Override
    @NotNull
    public Task findByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        return taskRepository.findByName(userId, name);
    }

    @Override
    public void removeByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        taskRepository.removeByName(userId, name);
    }

    @Override
    public void updateById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkId(id);
        checkName(name);
        taskRepository.updateById(userId, id, name, description);
    }

    @Override
    public void updateByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final String name,
            @NotNull final String description
    ) throws AbstractException {
        checkIndex(index);
        checkName(name);
        taskRepository.updateByIndex(userId, index, name, description);
    }

    @Override
    public void startById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        taskRepository.startById(userId, id);
    }

    @Override
    public void startByIndex(@NotNull final String userId, final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.startByIndex(userId, index);
    }

    @Override
    public void startByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        taskRepository.startByName(userId, name);
    }

    @Override
    public void finishById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        checkId(id);
        taskRepository.finishById(userId, id);
    }

    @Override
    public void finishByIndex(@NotNull final String userId, final int index) throws AbstractException {
        checkIndex(index);
        taskRepository.finishByIndex(userId, index);
    }

    @Override
    public void finishByName(@NotNull final String userId, @NotNull final String name) throws AbstractException {
        checkName(name);
        taskRepository.finishByName(userId, name);
    }

    @Override
    public void updateStatusById(
            @NotNull final String userId,
            @NotNull final String id,
            @NotNull final Status status
    ) throws AbstractException {
        checkId(id);
        taskRepository.updateStatusById(userId, id, status);
    }

    @Override
    public void updateStatusByIndex(
            @NotNull final String userId,
            final int index,
            @NotNull final Status status
    ) throws AbstractException {
        checkIndex(index);
        taskRepository.updateStatusByIndex(userId, index, status);
    }

    @Override
    public void updateStatusByName(
            @NotNull final String userId,
            @NotNull final String name,
            @NotNull final Status status
    ) throws AbstractException {
        checkName(name);
        taskRepository.updateStatusByName(userId, name, status);
    }

    private void checkName(@NotNull final String name) throws EmptyNameException {
        if (EmptyUtil.isEmpty(name)) throw new EmptyNameException();
    }

    private void checkId(@NotNull final String id) throws EmptyIdException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
    }

    private void checkIndex(final int index) throws IndexIncorrectException {
        if (index < 0) throw new IndexIncorrectException();
    }

}