package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

public abstract class AbstractOwnerRepository<E extends AbstractOwnerEntity> extends AbstractRepository<E> implements IOwnerRepository<E> {

    @Override
    public boolean existById(
            @NotNull final String userId,
            @NotNull final String id
    ) throws EntityNotFoundException {
        findById(userId, id);
        return true;
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) {
        entity.setUserId(userId);
        entities.add(entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) {
        @NotNull final List<E> ownerEntities = findAll(userId);
        ownerEntities.remove(entity);
    }

    @Override
    public void clear(@NotNull final String userId) {
        @NotNull final List<E> ownerEntities = findAll(userId);
        for (E entity : ownerEntities) entities.remove(entity);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return entities.stream()
                .filter(e -> e.getUserId().equals(userId))
                .sorted(comparator)
                .collect(Collectors.toList());
    }

    @Override
    @NotNull
    public E findById(@NotNull final String userId, @NotNull final String id) throws EntityNotFoundException {
        return findAll(userId).stream()
                .filter(e -> e.getId().equals(id))
                .findFirst()
                .orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @NotNull
    public E findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        @NotNull final List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IndexIncorrectException();
        return ownerEntities.get(index);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        @NotNull final Optional<E> optional = Optional.of(findById(userId, id));
        optional.ifPresent(this::remove);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) throws AbstractException {
        @NotNull final List<E> ownerEntities = findAll(userId);
        if (ownerEntities.size() - 1 < index) throw new IndexIncorrectException();
        @NotNull final E entity = ownerEntities.get(index);
        ownerEntities.remove(entity);
    }

}