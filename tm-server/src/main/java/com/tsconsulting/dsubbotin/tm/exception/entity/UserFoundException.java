package com.tsconsulting.dsubbotin.tm.exception.entity;

import com.tsconsulting.dsubbotin.tm.exception.AbstractException;

public final class UserFoundException extends AbstractException {

    public UserFoundException() {
        super("User found!");
    }

}