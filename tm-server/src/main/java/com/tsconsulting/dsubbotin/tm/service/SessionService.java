package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public final class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IServiceLocator serviceLocator;

    public SessionService(
            @NotNull final ISessionRepository sessionRepository,
            @NotNull final IServiceLocator serviceLocator
    ) {
        super(sessionRepository);
        this.sessionRepository = sessionRepository;
        this.serviceLocator = serviceLocator;
    }

    @NotNull
    @Override
    public Session open(
            @NotNull final String login,
            @NotNull final String password
    ) throws AbstractException {
        @NotNull final User user = serviceLocator.getUserService().findByLogin(login);
        final int iteration = serviceLocator.getPropertyService().getPasswordIteration();
        @NotNull final String secret = serviceLocator.getPropertyService().getPasswordSecret();
        @NotNull final String hash = HashUtil.salt(iteration, secret, password);
        if (!hash.equals(user.getPasswordHash())) throw new AccessDeniedException();
        final Session session = new Session();
        session.setUserId(user.getId());
        session.setDate(new Date());
        session.setSignature(null);
        sessionRepository.open(sign(session));
        return session;
    }

    @Override
    public boolean close(@NotNull final Session session) {
        return sessionRepository.close(session);
    }

    @Override
    public void validate(
            @Nullable final Session session,
            @NotNull final Role role
    ) throws AbstractException {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final Role userRole = serviceLocator.getUserService().findById(userId).getRole();
        if (!userRole.equals(role)) throw new AccessDeniedException();
    }

    @Override
    public void validate(@Nullable final Session session) throws AccessDeniedException {
        checkSession(session);
        @Nullable final Session sessionTemp = session.clone();
        if (sessionTemp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @Nullable final String signatureTarget = sign(sessionTemp).getSignature();
        if (!signatureSource.equals(signatureTarget)) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @NotNull
    @Override
    public User getUser(@NotNull final Session session) throws AbstractException {
        @NotNull final String userId = session.getUserId();
        return serviceLocator.getUserService().findById(userId);
    }

    @NotNull
    @Override
    public String getUserId(@NotNull final Session session) {
        return session.getUserId();
    }

    @NotNull
    private Session sign(@NotNull final Session session) {
        session.setSignature(null);
        final int iteration = serviceLocator.getPropertyService().getSignatureIteration();
        @NotNull String secret = serviceLocator.getPropertyService().getSignatureSecret();
        @Nullable final String signature = HashUtil.sign(iteration, secret, session);
        session.setSignature(signature);
        return session;
    }

    private void checkSession(@Nullable final Session session) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getDate().toString())) throw new AccessDeniedException();
        if (EmptyUtil.isEmpty(session.getSignature())) throw new AccessDeniedException();
    }

}
