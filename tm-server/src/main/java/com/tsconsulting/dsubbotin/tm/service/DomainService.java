package com.tsconsulting.dsubbotin.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.tsconsulting.dsubbotin.tm.api.service.IDomainService;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.dto.Domain;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyDomainException;
import com.tsconsulting.dsubbotin.tm.util.DateAdapter;
import lombok.AllArgsConstructor;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;
import java.util.Date;

@AllArgsConstructor
public final class DomainService implements IDomainService {

    @NotNull
    private static final String FILE_BINARY = "./data.bin";

    @NotNull
    private static final String FILE_BASE64 = "./data.base64";

    @NotNull
    private static final String FILE_XML_FASTERXML = "./data_fasterxml.xml";

    @NotNull
    private static final String FILE_JSON_FASTERXML = "./data_fasterxml.json";

    @NotNull
    private static final String FILE_XML_JAXB = "./data_jaxb.xml";

    @NotNull
    private static final String FILE_JSON_JAXB = "./data_jaxb.json";

    @NotNull
    private static final String BACKUP_XML = "./backup.xml";

    @NotNull
    private final IServiceLocator serviceLocator;

    @Override
    public void dataLoadJsonJaxb() throws Exception {
        @NotNull final Unmarshaller jaxbUnmarshaller = getJaxbUnmarshaller();
        jaxbUnmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public void dataSaveJsonJaxb() throws Exception {
        @NotNull final Marshaller jaxbMarshaller = getJaxbMarshaller();
        jaxbMarshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        @NotNull final File file = new File(FILE_JSON_JAXB);
        @NotNull final Domain domain = getDomain();
        jaxbMarshaller.marshal(domain, file);
    }

    @Override
    public void dataLoadXmlJaxb() throws Exception {
        @NotNull final Unmarshaller jaxbUnmarshaller = getJaxbUnmarshaller();
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final Domain domain = (Domain) jaxbUnmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    public void dataSaveXmlJaxb() throws Exception {
        @NotNull final Marshaller jaxbMarshaller = getJaxbMarshaller();
        @NotNull final File file = new File(FILE_XML_JAXB);
        @NotNull final Domain domain = getDomain();
        jaxbMarshaller.marshal(domain, file);
    }

    @Override
    public void dataLoadJsonFasterXml() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new JsonMapper());
        @NotNull final File file = new File(FILE_JSON_FASTERXML);
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    public void dataSaveJsonFasterXml() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new JsonMapper());
        @NotNull final File file = new File(FILE_JSON_FASTERXML);
        @NotNull final Domain domain = getDomain();
        objectMapper.writeValue(file, domain);
    }

    @Override
    public void dataLoadXmlFasterXml() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        @NotNull final File file = new File(FILE_XML_FASTERXML);
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    public void dataSaveXmlFasterXml() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final File file = new File(FILE_XML_FASTERXML);
        @NotNull final Domain domain = getDomain();
        objectMapper.writeValue(file, domain);
    }

    @Override
    public void loadBackup() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        @NotNull final File file = new File(BACKUP_XML);
        if (!file.canRead()) return;
        @NotNull final Domain domain = objectMapper.readValue(file, Domain.class);
        setDomain(domain);
    }

    @Override
    public void saveBackup() throws Exception {
        @NotNull final ObjectMapper objectMapper = getObjectMapper(new XmlMapper());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        @NotNull final File file = new File(BACKUP_XML);
        @NotNull final Domain domain = getDomain();
        objectMapper.writeValue(file, domain);
    }

    @Override
    public void loadBinary() throws Exception {
        @NotNull final InputStream fileInputStream = new FileInputStream(FILE_BINARY);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    public void saveBinary() throws Exception {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BINARY);
        Files.delete(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final OutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    public void loadBase64() throws Exception {
        @NotNull final byte[] base64Data = Files.readAllBytes(Paths.get(FILE_BASE64));
        @NotNull final byte[] bytesData = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytesData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    public void saveBase64() throws Exception {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        @NotNull final byte[] bytesData = byteArrayOutputStream.toByteArray();
        @NotNull final String base64Data = new String(Base64.getEncoder().encode(bytesData));
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(FILE_BASE64);
        fileOutputStream.write(base64Data.getBytes());
        fileOutputStream.close();
    }

    @NotNull
    private Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setDate(new Date());
        domain.setUsers(serviceLocator.getUserService().findAll());
        domain.setProjects(serviceLocator.getProjectService().findAll());
        domain.setTasks(serviceLocator.getTaskService().findAll());
        return domain;
    }

    private void setDomain(@Nullable Domain domain) throws AbstractException {
        if (domain == null) throw new EmptyDomainException();
        serviceLocator.getUserService().clear();
        serviceLocator.getUserService().addAll(domain.getUsers());
        serviceLocator.getProjectService().clear();
        serviceLocator.getProjectService().addAll(domain.getProjects());
        serviceLocator.getTaskService().clear();
        serviceLocator.getTaskService().addAll(domain.getTasks());
    }

    @NotNull
    private Unmarshaller getJaxbUnmarshaller() throws Exception {
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(
                new Class[]{Domain.class},
                null);
        @NotNull final Unmarshaller jaxbUnmarshaller = jaxbContext.createUnmarshaller();
        return jaxbUnmarshaller;
    }

    @NotNull
    private Marshaller getJaxbMarshaller() throws Exception {
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(
                new Class[]{Domain.class},
                null);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        return jaxbMarshaller;
    }

    @NotNull
    private ObjectMapper getObjectMapper(final ObjectMapper objMapper) {
        @NotNull final ObjectMapper objectMapper = objMapper;
        objectMapper.setDateFormat(DateAdapter.getSimpleDateFormat());
        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);
        objectMapper.enable(SerializationFeature.WRAP_ROOT_VALUE);
        return objMapper;
    }

}
