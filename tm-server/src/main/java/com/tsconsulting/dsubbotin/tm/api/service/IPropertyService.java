package com.tsconsulting.dsubbotin.tm.api.service;

import org.jetbrains.annotations.NotNull;

public interface IPropertyService {

    @NotNull
    String getApplicationVersion();

    @NotNull
    String getDeveloperName();

    @NotNull
    String getDeveloperEmail();

    int getPasswordIteration();

    @NotNull
    String getPasswordSecret();

    int getSignatureIteration();

    @NotNull
    String getSignatureSecret();

    @NotNull
    String getServerHost();

    @NotNull
    String getServerPort();

}
