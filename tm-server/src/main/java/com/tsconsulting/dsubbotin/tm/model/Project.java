package com.tsconsulting.dsubbotin.tm.model;

import com.tsconsulting.dsubbotin.tm.api.entity.IWBS;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.util.DateAdapter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.adapters.XmlJavaTypeAdapter;
import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@XmlRootElement(name = "Project")
@XmlAccessorType(XmlAccessType.FIELD)
public final class Project extends AbstractOwnerEntity implements IWBS {

    @NotNull
    private String name;

    @Nullable
    private String description;

    @NotNull
    private Status status = Status.NOT_STARTED;

    @Nullable
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date startDate = null;

    @NotNull
    @XmlElement
    @XmlJavaTypeAdapter(DateAdapter.class)
    private Date createDate = new Date();

    @Override
    @NotNull
    public String toString() {
        return super.toString() + name + "; " +
                "Status: " + status + "; " +
                "Started: " + startDate + "; " +
                "Created: " + createDate + ";";
    }

}