package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.entity.UserNotFoundException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;

public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    @NotNull
    public User findByLogin(@NotNull final String login) throws AbstractException {
        return entities.stream()
                .filter(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()))
                .findFirst()
                .orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void removeByLogin(@NotNull final String login) throws AbstractException {
        @NotNull final User user = findByLogin(login);
        entities.remove(user);
    }

    @Override
    public boolean isLogin(@NotNull final String login) {
        return entities.stream()
                .anyMatch(u -> u.getLogin().toLowerCase().equals(login.toLowerCase()));
    }

}
