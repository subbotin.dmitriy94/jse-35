package com.tsconsulting.dsubbotin.tm.api.service;

import com.tsconsulting.dsubbotin.tm.enumerated.Role;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.system.AccessDeniedException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public interface ISessionService {

    @NotNull
    Session open(
            @NotNull String login,
            @NotNull String password
    ) throws AbstractException;

    boolean close(@NotNull Session session);

    void validate(@Nullable Session session) throws AccessDeniedException;

    void validate(@Nullable Session session, @NotNull Role role) throws AbstractException;

    @NotNull
    User getUser(@NotNull Session session) throws AbstractException;

    @NotNull
    String getUserId(@NotNull Session session);

}
