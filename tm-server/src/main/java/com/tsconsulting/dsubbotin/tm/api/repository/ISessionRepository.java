package com.tsconsulting.dsubbotin.tm.api.repository;

import com.tsconsulting.dsubbotin.tm.model.Session;
import org.jetbrains.annotations.NotNull;

public interface ISessionRepository extends IRepository<Session> {

    void open(@NotNull final Session session);

    boolean close(@NotNull final Session session);

    boolean contains(@NotNull final String id);

}
