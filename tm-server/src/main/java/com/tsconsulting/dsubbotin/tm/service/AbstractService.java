package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IService;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.model.AbstractEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import lombok.AllArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.Comparator;
import java.util.List;

@AllArgsConstructor
public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NotNull
    protected final IRepository<E> repository;

    @Override
    public void add(@NotNull final E entity) throws AbstractException {
        repository.add(entity);
    }

    @Override
    public void addAll(@NotNull List<E> entities) {
        repository.addAll(entities);
    }

    @Override
    public void remove(@NotNull final E entity) throws AbstractException {
        repository.remove(entity);
    }

    @Override
    public void clear() {
        repository.clear();
    }

    @Override
    @NotNull
    public List<E> findAll() {
        return repository.findAll();
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final Comparator<E> comparator) {
        return repository.findAll(comparator);
    }

    @Override
    @NotNull
    public E findById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        return repository.findById(id);
    }

    @Override
    @NotNull
    public E findByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        return repository.findByIndex(index);
    }

    @Override
    public void removeById(@NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        repository.removeById(id);
    }

    @Override
    public void removeByIndex(final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        repository.removeByIndex(index);
    }

}
