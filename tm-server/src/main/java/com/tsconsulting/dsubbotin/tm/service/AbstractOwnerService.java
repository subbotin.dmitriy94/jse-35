package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IOwnerRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IOwnerService;
import com.tsconsulting.dsubbotin.tm.enumerated.Sort;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.exception.empty.EmptyIdException;
import com.tsconsulting.dsubbotin.tm.exception.entity.EntityNotFoundException;
import com.tsconsulting.dsubbotin.tm.exception.system.IndexIncorrectException;
import com.tsconsulting.dsubbotin.tm.exception.system.UnknownSortException;
import com.tsconsulting.dsubbotin.tm.model.AbstractOwnerEntity;
import com.tsconsulting.dsubbotin.tm.util.EmptyUtil;
import com.tsconsulting.dsubbotin.tm.util.EnumerationUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    private final IOwnerRepository<E> ownerRepository;

    public AbstractOwnerService(IOwnerRepository<E> ownerRepository) {
        super(ownerRepository);
        this.ownerRepository = ownerRepository;
    }

    @Override
    public boolean existById(@NotNull final String userId, @NotNull final String id) throws EntityNotFoundException {
        return ownerRepository.existById(userId, id);
    }

    @Override
    public void add(@NotNull final String userId, @NotNull final E entity) throws AbstractException {
        ownerRepository.add(userId, entity);
    }

    @Override
    public void remove(@NotNull final String userId, @NotNull final E entity) throws AbstractException {
        ownerRepository.remove(userId, entity);
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @Nullable final String sort) {
        try {
            @NotNull final Sort sortType = EnumerationUtil.parseSort(sort);
            return findAll(userId, sortType.getComparator());
        } catch (UnknownSortException e) {
            return ownerRepository.findAll(userId);
        }
    }

    @Override
    @NotNull
    public List<E> findAll(@NotNull final String userId, @NotNull final Comparator<E> comparator) {
        return ownerRepository.findAll(userId, comparator);
    }

    @Override
    @NotNull
    public E findById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        return ownerRepository.findById(userId, id);
    }

    @Override
    @NotNull
    public E findByIndex(@NotNull final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        return ownerRepository.findByIndex(userId, index);
    }

    @Override
    public void removeById(@NotNull final String userId, @NotNull final String id) throws AbstractException {
        if (EmptyUtil.isEmpty(id)) throw new EmptyIdException();
        ownerRepository.removeById(userId, id);
    }

    @Override
    public void removeByIndex(@NotNull final String userId, final int index) throws AbstractException {
        if (index < 0) throw new IndexIncorrectException();
        ownerRepository.removeByIndex(userId, index);
    }

}
