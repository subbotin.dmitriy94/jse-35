package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class TaskRepositoryTest {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    private final IProjectRepository projectRepository;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "testTask";

    @NotNull
    private final String taskDescription = "testTask";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    @NotNull
    private final String userId;

    public TaskRepositoryTest() {
        taskRepository = new TaskRepository();
        @NotNull final User user = new User();
        userId = user.getId();
        user.setLogin("guest");
        user.setPasswordHash(HashUtil.salt(3, "qwe", "guest"));
        task = new Task();
        taskId = task.getId();
        task.setUserId(userId);
        task.setName(taskName);
        task.setDescription(taskDescription);
        projectRepository = new ProjectRepository();
        project = new Project();
        projectId = project.getId();
        project.setUserId(userId);
    }

    @Before
    public void initializeTest() throws AbstractException {
        taskRepository.add(task);
        projectRepository.add(project);
    }

    @Test
    public void findTask() throws AbstractException {
        Assert.assertEquals(task, taskRepository.findByName(userId, taskName));
        Assert.assertEquals(task, taskRepository.findById(taskId));
        Assert.assertEquals(task, taskRepository.findById(userId, taskId));
        Assert.assertEquals(task, taskRepository.findByIndex(0));
    }

    @Test
    public void removeByName() throws AbstractException {
        Assert.assertNotNull(task);
        taskRepository.removeByName(userId, taskName);
        Assert.assertTrue(taskRepository.findAll().isEmpty());
    }

    @Test
    public void updateById() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateById(userId, taskId, newTaskName, newTaskDescription);
        @NotNull final Task updatedTask = taskRepository.findById(userId, taskId);
        Assert.assertEquals(task, updatedTask);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void updateByIndex() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskRepository.updateByIndex(userId, 0, newTaskName, newTaskDescription);
        @NotNull final Task updatedTask = taskRepository.findById(userId, taskId);
        Assert.assertEquals(task, updatedTask);
        Assert.assertEquals(newTaskName, updatedTask.getName());
        Assert.assertEquals(newTaskDescription, updatedTask.getDescription());
        Assert.assertNotEquals(taskName, updatedTask.getName());
        Assert.assertNotEquals(taskDescription, updatedTask.getDescription());
    }

    @Test
    public void startById() throws AbstractException {
        taskRepository.startById(userId, taskId);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        taskRepository.startByIndex(userId, 0);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        taskRepository.startByName(userId, taskName);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        taskRepository.finishById(userId, taskId);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        taskRepository.finishByIndex(userId, 0);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        taskRepository.finishByName(userId, taskName);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        taskRepository.updateStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findById(userId, taskId).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        taskRepository.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findByIndex(userId, 0).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        taskRepository.updateStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.IN_PROGRESS);
        taskRepository.updateStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.COMPLETED);
        taskRepository.updateStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(taskRepository.findByName(userId, taskName).getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProjectById(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void unbindTaskById(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(projectId, taskRepository.findById(userId, taskId).getProjectId());
        taskRepository.unbindTaskById(userId, taskId);
        Assert.assertNull(taskRepository.findById(userId, taskId).getProjectId());
    }

    @Test
    public void findAllByProjectId(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        Assert.assertEquals(task, taskRepository.findAllByProjectId(userId, projectId).get(0));
    }

    @Test
    public void removeAllTaskByProjectId(
    ) throws AbstractException {
        taskRepository.bindTaskToProjectById(userId, projectId, taskId);
        @NotNull final Task task1 = new Task();
        task1.setUserId(userId);
        taskRepository.add(userId, task1);
        taskRepository.bindTaskToProjectById(userId, projectId, task1.getId());
        Assert.assertEquals(2, taskRepository.findAllByProjectId(userId, projectId).size());
        taskRepository.removeAllTaskByProjectId(userId, projectId);
        Assert.assertEquals(0, taskRepository.findAllByProjectId(userId, projectId).size());
    }

    @After
    public void finalizeTest() {
        taskRepository.clear(userId);
        projectRepository.clear(userId);
    }

}
