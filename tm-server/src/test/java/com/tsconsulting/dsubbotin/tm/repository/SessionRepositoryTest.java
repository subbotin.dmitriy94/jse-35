package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.ISessionRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class SessionRepositoryTest {

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final Session session;

    @NotNull
    private final String sessionId;

    public SessionRepositoryTest() {
        sessionRepository = new SessionRepository();
        session = new Session();
        sessionId = session.getId();
    }

    @Before
    public void initializeTest() {
        session.setUserId(new User().getId());
    }

    @Test
    public void openClose() throws AbstractException {
        sessionRepository.open(session);
        Assert.assertEquals(session, sessionRepository.findById(sessionId));
        sessionRepository.close(session);
        Assert.assertTrue(sessionRepository.findAll().isEmpty());
    }

    @Test
    public void contains() {
        sessionRepository.open(session);
        Assert.assertTrue(sessionRepository.contains(sessionId));
    }

    @After
    public void finalizeTest() {
        sessionRepository.clear();
    }

}
