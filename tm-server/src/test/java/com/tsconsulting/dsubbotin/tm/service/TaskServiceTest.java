package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.repository.IProjectRepository;
import com.tsconsulting.dsubbotin.tm.api.repository.ITaskRepository;
import com.tsconsulting.dsubbotin.tm.api.service.IProjectTaskService;
import com.tsconsulting.dsubbotin.tm.api.service.ITaskService;
import com.tsconsulting.dsubbotin.tm.enumerated.Status;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Project;
import com.tsconsulting.dsubbotin.tm.model.Task;
import com.tsconsulting.dsubbotin.tm.repository.ProjectRepository;
import com.tsconsulting.dsubbotin.tm.repository.TaskRepository;
import org.jetbrains.annotations.NotNull;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public final class TaskServiceTest {

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IProjectTaskService projectTaskService;

    @NotNull
    private final Task task;

    @NotNull
    private final String taskId;

    @NotNull
    private final String taskName = "taskName";

    @NotNull
    private final String taskDescription = "taskDescription";

    @NotNull
    private final String userId = "userId";

    @NotNull
    private final Project project;

    @NotNull
    private final String projectId;

    public TaskServiceTest() throws AbstractException {
        task = new Task();
        taskId = task.getId();
        task.setName(taskName);
        task.setDescription(taskDescription);
        task.setUserId(userId);
        @NotNull final ITaskRepository taskRepository = new TaskRepository();
        taskRepository.add(userId, task);
        taskService = new TaskService(taskRepository);
        @NotNull final IProjectRepository projectRepository = new ProjectRepository();
        project = new Project();
        projectId = project.getId();
        project.setName("project");
        project.setDescription("project");
        projectRepository.add(userId, project);
        projectTaskService = new ProjectTaskService(projectRepository, taskRepository);
    }

    @Test
    public void create() throws AbstractException {
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        @NotNull final Task newTask = taskService.create(userId, newTaskName, newTaskDescription);
        Assert.assertEquals(2, taskService.findAll().size());
        Assert.assertEquals(newTask.getName(), newTaskName);
        Assert.assertEquals(newTask.getDescription(), newTaskDescription);
    }

    @Test
    public void findByName() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final Task task = taskService.findByName(userId, taskName);
        Assert.assertEquals(task.getName(), taskName);
        Assert.assertEquals(task.getDescription(), taskDescription);
        Assert.assertEquals(task.getUserId(), userId);
    }

    @Test
    public void updateById() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateById(userId, taskId, newTaskName, newTaskDescription);
        Assert.assertEquals(task, taskService.findById(userId, taskId));
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void updateByIndex() throws AbstractException {
        Assert.assertEquals(task, taskService.findByName(userId, taskName));
        @NotNull final String newTaskName = "newTaskName";
        @NotNull final String newTaskDescription = "newTaskDescription";
        taskService.updateByIndex(userId, 0, newTaskName, newTaskDescription);
        Assert.assertEquals(task, taskService.findByIndex(userId, 0));
        @NotNull final Task tempTask = taskService.findByName(userId, newTaskName);
        Assert.assertNotEquals(tempTask.getName(), taskName);
        Assert.assertNotEquals(tempTask.getDescription(), taskDescription);
        Assert.assertEquals(tempTask.getName(), newTaskName);
        Assert.assertEquals(tempTask.getDescription(), newTaskDescription);
        Assert.assertEquals(tempTask.getId(), taskId);
        Assert.assertEquals(tempTask.getUserId(), userId);
    }

    @Test
    public void startById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startById(userId, taskId);
        Assert.assertNotNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(userId, 0);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByIndex(userId, 0);
        Assert.assertNotNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void startByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.startByName(userId, taskName);
        Assert.assertNotNull(tempTask.getStartDate());
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
    }

    @Test
    public void finishById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.finishById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(userId, 0);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.finishByIndex(userId, 0);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
    }

    @Test
    public void finishByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.finishByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
    }

    @Test
    public void updateStatusById() throws AbstractException {
        @NotNull final Task tempTask = taskService.findById(userId, taskId);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.updateStatusById(userId, taskId, Status.IN_PROGRESS);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusById(userId, taskId, Status.COMPLETED);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
        taskService.updateStatusById(userId, taskId, Status.NOT_STARTED);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByIndex() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByIndex(userId, 0);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByIndex(userId, 0, Status.IN_PROGRESS);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByIndex(userId, 0, Status.COMPLETED);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
        taskService.updateStatusByIndex(userId, 0, Status.NOT_STARTED);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void updateStatusByName() throws AbstractException {
        @NotNull final Task tempTask = taskService.findByName(userId, taskName);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
        taskService.updateStatusByName(userId, taskName, Status.IN_PROGRESS);
        Assert.assertEquals(tempTask.getStatus(), Status.IN_PROGRESS);
        taskService.updateStatusByName(userId, taskName, Status.COMPLETED);
        Assert.assertEquals(tempTask.getStatus(), Status.COMPLETED);
        taskService.updateStatusByName(userId, taskName, Status.NOT_STARTED);
        Assert.assertEquals(tempTask.getStatus(), Status.NOT_STARTED);
    }

    @Test
    public void bindTaskToProject() throws AbstractException {
        Assert.assertNull(task.getProjectId());
        projectTaskService.bindTaskToProject(userId, projectId, taskId);
        Assert.assertNotNull(task.getProjectId());
        Assert.assertEquals(task.getProjectId(), projectId);
    }

    @Test
    public void unbindTaskFromProject() throws AbstractException {
        bindTaskToProject();
        projectTaskService.unbindTaskFromProject(userId, projectId, taskId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void findAllTasksByProjectId() throws AbstractException {
        bindTaskToProject();
        @NotNull final List<Task> tasks = projectTaskService.findAllTasksByProjectId(userId, projectId);
        Assert.assertFalse(tasks.isEmpty());
        Assert.assertEquals(1, tasks.size());
        Assert.assertEquals(task, tasks.get(0));
    }

    @Test
    public void removeProjectById() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(task.getProjectId());
        projectTaskService.removeProjectById(userId, projectId);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeProjectByIndex() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(task.getProjectId());
        projectTaskService.removeProjectByIndex(userId, 0);
        Assert.assertNull(task.getProjectId());
    }

    @Test
    public void removeProjectByName() throws AbstractException {
        bindTaskToProject();
        Assert.assertNotNull(task.getProjectId());
        project.setName("projectName");
        projectTaskService.removeProjectByName(userId, project.getName());
        Assert.assertNull(task.getProjectId());
    }

}
