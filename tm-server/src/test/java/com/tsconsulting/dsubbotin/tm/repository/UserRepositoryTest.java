package com.tsconsulting.dsubbotin.tm.repository;

import com.tsconsulting.dsubbotin.tm.api.repository.IUserRepository;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.User;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class UserRepositoryTest {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    private final User user;

    @NotNull
    private final String userLogin = "userTest";

    public UserRepositoryTest() {
        userRepository = new UserRepository();
        user = new User();
        user.setLogin(userLogin);
    }

    @Before
    public void initializeTest() throws AbstractException {
        userRepository.add(user);
    }

    @Test
    public void findByLogin() throws AbstractException {
        Assert.assertEquals(user, userRepository.findByLogin(userLogin));
    }

    @Test
    public void removeByLogin() throws AbstractException {
        userRepository.removeByLogin(userLogin);
        Assert.assertTrue(userRepository.findAll().isEmpty());
    }

    @Test
    public void isLogin() {
        Assert.assertTrue(userRepository.isLogin(userLogin));
    }

    @After
    public void finalizeTest() {
        userRepository.clear();
    }

}
