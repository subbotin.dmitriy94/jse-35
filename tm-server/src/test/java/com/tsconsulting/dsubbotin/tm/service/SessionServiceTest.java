package com.tsconsulting.dsubbotin.tm.service;

import com.tsconsulting.dsubbotin.tm.api.service.IPropertyService;
import com.tsconsulting.dsubbotin.tm.api.service.IServiceLocator;
import com.tsconsulting.dsubbotin.tm.api.service.ISessionService;
import com.tsconsulting.dsubbotin.tm.component.Bootstrap;
import com.tsconsulting.dsubbotin.tm.exception.AbstractException;
import com.tsconsulting.dsubbotin.tm.model.Session;
import com.tsconsulting.dsubbotin.tm.model.User;
import com.tsconsulting.dsubbotin.tm.util.HashUtil;
import org.jetbrains.annotations.NotNull;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public final class SessionServiceTest {

    @NotNull
    private final ISessionService sessionService;

    @NotNull
    private final IPropertyService propertyService;

    @NotNull
    private final User user;

    @NotNull
    private final String userId;

    @NotNull
    private final String userLogin = "userLogin";

    @NotNull
    private final String userPassword = "userPassword";

    @NotNull
    private Session session;

    public SessionServiceTest() throws AbstractException {
        @NotNull final IServiceLocator serviceLocator = new Bootstrap();
        sessionService = serviceLocator.getSessionService();
        propertyService = serviceLocator.getPropertyService();
        user = new User();
        userId = user.getId();
        user.setLogin(userLogin);
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        user.setPasswordHash(HashUtil.salt(iteration, secret, userPassword));
        serviceLocator.getUserService().add(user);
    }

    @Before
    public void initializeTest() throws AbstractException {
        session = sessionService.open(userLogin, userPassword);
    }

    @Test
    public void open() {
        Assert.assertNotNull(session);
        Assert.assertEquals(userId, session.getUserId());
    }

    @Test
    public void close() {
        Assert.assertEquals(userId, session.getUserId());
        Assert.assertTrue(sessionService.close(session));
        Assert.assertFalse(sessionService.close(session));
    }

    @Test
    public void validate() throws AbstractException {
        sessionService.validate(session);
    }

    @Test
    public void getUser() throws AbstractException {
        @NotNull final User tempUser = sessionService.getUser(session);
        Assert.assertEquals(user, tempUser);
        Assert.assertEquals(userId, tempUser.getId());
        Assert.assertEquals(userLogin, tempUser.getLogin());
        final int iteration = propertyService.getPasswordIteration();
        @NotNull final String secret = propertyService.getPasswordSecret();
        @NotNull final String passwordHash = HashUtil.salt(iteration, secret, userPassword);
        Assert.assertEquals(passwordHash, tempUser.getPasswordHash());
    }

    @Test
    public void getUserId() {
        Assert.assertEquals(userId, sessionService.getUserId(session));
    }

    @After
    public void finalizeTest() {
        sessionService.close(session);
    }

}
